﻿using System;
using Lexical;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestLexical
{
    [TestClass]
    public class TestBook
    {
        [TestMethod]
        public void ConcatenateTwoWordsSuccess()
        {
            //given (contexte de départ)
            string word1 = "roberto";
            string word2 = "carlos";
            string expectedResult = "Roberto Carlos";
            string actualResult = "";
            Book book = new Book();

            //when (événement)
            actualResult = book.Concat(word1, word2);

            //then (résultat)
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
